from django import forms


class SubscriberForms(forms.Form):
    error_messages = {
        'required': 'Required Field'
    }

    name = forms.CharField(
        label='Name',
        required=True,
        widget=forms.TextInput(attrs={
            'type': 'text',
            'class': 'form-control',
            'placeholder': 'Required'
        })
    )

    email = forms.EmailField(
        label='Email',
        required=True,
        max_length=30,
        widget=forms.EmailInput(attrs={
            'type': 'email',
            'class': 'form-control',
            'placeholder': 'Required'
        })
    )

    password = forms.CharField(
        label='Password',
        required=True,
        widget=forms.PasswordInput(attrs={
            'type': 'password',
            'class': 'form-control',
            'placeholder': 'Required'
        })
    )
