from django.shortcuts import render, redirect
from django.http import JsonResponse
import json
import requests
from django.contrib.auth import logout
# Create your views here.


def list(request):
    return render(request, 'page/story9.html', {})


def list_json(request):
    url = 'https://enterkomputer.com/api/product/notebook.json'
    data_raw = json.loads(requests.get(url).text)
    jsonpc = []
    for i in data_raw:
        if "AMD" in i['details']:
            jsonpc.append(i)

    pc_list = []
    for i in range(0, 100):
        pc_dict = {"id": jsonpc[i]["id"], "name": jsonpc[i]["name"], "details": jsonpc[i]
                   ["details"], "brand": jsonpc[i]["brand"], "price": jsonpc[i]["price"]}
        pc_list.append(pc_dict)

    return JsonResponse({'data': pc_list})


def logout_view(request):
    request.session.flush()
    logout(request)
    return redirect('/list')
