from django import forms
from .models import Message

class MessageForm(forms.Form):
    your_message = forms.CharField(widget=forms.TextInput(attrs={
        'class':'form',
        'id':'pesan',
        'required':'True',
        'placeholder':'Apa kabar?',
    }))
